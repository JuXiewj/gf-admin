package sys_menu_controller

import (
	"gf-admin/app/pojo/sys/sys_menu"
	"gf-admin/app/service/sys/sys_menu_service"
	"gf-admin/app/service/sys/sys_dict_type_service"
	"gf-admin/app/service/cache_service"
	"gf-admin/library/response"
	"gf-admin/library/utils"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/gvalid"
)

//菜单用户组用户管理
type Auth struct{}

// @Summary 分页列表
// @Description 获取菜单列表
// @Tags 菜单管理
// @Param data body auth_rule.ReqSearch true "data"
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/auth/menuList [get]
// @Security
func (c *Auth) MenuList(r *ghttp.Request) {
	var req *sys_menu.ReqSearch
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	var listEntities []*sys_menu.Entity
	var err error
	if req != nil {
		listEntities, err = sys_menu_service.GetMenuListSearch(req)
	} else {
		//获取菜单信息
		listEntities, err = sys_menu_service.GetMenuList()
	}
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取数据失败")
	}
	list := gconv.SliceMap(listEntities)
	if req != nil {
		for k := range list {
			list[k]["children"] = nil
		}
	} else {
		list = utils.PushSonToParent(list, 0, "pid", "id", "children", "", nil, true)
	}
	//菜单显示状态
	visibleOptions, err := sys_dict_type_service.GetDictWithDataByType("sys_show_hide", "", "")
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	//菜单正常or停用状态
	statusOptions, err := sys_dict_type_service.GetDictWithDataByType("sys_normal_disable", "", "")
	if err != nil {
		response.FailJson(true, r, err.Error())
	}

	response.SusJson(true, r, "成功", g.Map{
		"list":           list,
		"visibleOptions": visibleOptions,
		"statusOptions":  statusOptions,
	})
}

//菜单排序
func (c *Auth) MenuSort(r *ghttp.Request) {
	sorts := r.Get("sorts")
	s := gconv.Map(sorts)
	if s == nil {
		response.FailJson(true, r, "排序失败")
	}
	for k, v := range s {
		sys_menu.Model.Where("id=?", k).Data("weigh", v).Update()
	}
	cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
	response.SusJson(true, r, "排序成功")
}

// @Summary 添加菜单
// @Description 添加菜单
// @Tags 菜单管理
// @Accept  application/json
// @Product application/json
// @Param data body auth_rule.MenuReq true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/addMenu [post]
// @Security Bearer
func (c *Auth) AddMenu(r *ghttp.Request) {
	if r.Method == "POST" {
		menu := new(sys_menu.MenuReq)
		if err := r.Parse(menu); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}
		//判断菜单规则是否存在
		if !sys_menu_service.CheckMenuNameUnique(menu.Name, 0) {
			response.FailJson(true, r, "菜单规则名称已经存在")
		}
		//判断路由是否已经存在
		if !sys_menu_service.CheckMenuPathUnique(menu.Path, 0) {
			response.FailJson(true, r, "路由地址已经存在")
		}
		//保存到数据库
		err, _ := sys_menu_service.AddMenu(menu)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "添加菜单失败")
		}
		//清除TAG缓存
		cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
		response.SusJson(true, r, "添加菜单成功")
	}
	//获取父级菜单信息
	listEntities, err := sys_menu_service.GetIsMenuList()
	if err != nil {
		response.FailJson(true, r, "获取数据失败")
	}
	response.SusJson(true, r, "成功", g.Map{"parentList": listEntities})
}

// @Summary 修改菜单
// @Description 修改菜单
// @Tags 菜单管理
// @Accept  application/json
// @Product application/json
// @Param data body auth_rule.MenuReq true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "修改成功"}"
// @Router /system/auth/editMenu [post]
// @Security Bearer
func (c *Auth) EditMenu(r *ghttp.Request) {
	id := r.GetInt("menuId")
	if r.Method == "POST" {
		menu := new(sys_menu.MenuReq)
		if err := r.Parse(menu); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}
		//判断菜单规则是否存在
		if !sys_menu_service.CheckMenuNameUnique(menu.Name, id) {
			response.FailJson(true, r, "菜单规则名称已经存在")
		}
		//判断路由是否已经存在
		if !sys_menu_service.CheckMenuPathUnique(menu.Path, id) {
			response.FailJson(true, r, "路由地址已经存在")
		}
		//保存到数据库
		err, _ := sys_menu_service.EditMenu(menu, id)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "修改菜单失败")
		}
		//清除TAG缓存
		cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
		response.SusJson(true, r, "修改菜单成功")
	}
	menuEntity, err := sys_menu.Model.Where("id=?", id).One()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取数据失败")
	}
	//获取父级菜单信息
	listEntities, err := sys_menu_service.GetIsMenuList()
	if err != nil {
		response.FailJson(true, r, "获取数据失败")
	}
	list := gconv.SliceMap(listEntities)
	list = utils.ParentSonSort(list)
	response.SusJson(true, r, "成功", g.Map{
		"parentList": list,
		"menu":       menuEntity,
	})
}

// @Summary 删除菜单
// @Description 删除菜单
// @Tags 菜单管理
// @Accept  application/json
// @Product application/json
// @Param ids body integer  true "ids[1,2,3]"
// @Success 200 {object} response.Response	"{"code": 0, "message": "修改成功"}"
// @Router /system/auth/deleteMenu [delete]
// @Security Bearer
func (c *Auth) DeleteMenu(r *ghttp.Request) {
	ids := r.GetInts("ids")
	if len(ids) == 0 {
		response.FailJson(true, r, "删除失败，参数错误")
	}
	err := sys_menu_service.DeleteMenuByIds(ids)
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "删除失败")
	}
	//清除TAG缓存
	cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
	response.SusJson(true, r, "删除成功")
}