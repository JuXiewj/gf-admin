package sys_user_controller

import (
	"fmt"
	"gf-admin/app/pojo/sys/sys_dept"
	"gf-admin/app/pojo/sys/sys_role"
	"gf-admin/app/pojo/sys/sys_user"
	"gf-admin/app/pojo/sys/sys_user_post"
	"gf-admin/app/service/casbin_adapter_service"
	"gf-admin/app/service/sys/sys_dept_service"
	"gf-admin/app/service/sys/sys_dict_type_service"
	"gf-admin/app/service/sys/sys_post_service"
	"gf-admin/app/service/sys/sys_role_service"
	"gf-admin/app/service/sys/sys_user_post_service"
	"gf-admin/app/service/sys/upload_service"
	"gf-admin/app/service/sys/sys_user_service"
	"gf-admin/library/response"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/gvalid"
)

type User struct{}

// @Summary 头像上传上传
// @Description 头像上传上传
// @Tags 个人中心
// @Param avatarfile body string  true "avatarfile"
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/user/avatar [post]
// @Security
func (c *User) Avatar(r *ghttp.Request) {
	upFile := r.GetUploadFile("avatarfile")
	info, err := upload_service.UpImg(upFile)
	if err != nil {
		response.FailJson(true, r, "上传失败，"+err.Error())
	}
	res := g.Map{
		"fileInfo": info,
	}
	id := sys_user_service.GetLoginID(r)
	if _, err := sys_user.Model.Where("id", id).Data(g.Map{
		"avatar": info.FileUrl,
	}).Update(); err != nil {
		response.FailJson(true, r, err.Error())
	}
	response.SusJson(true, r, "上传成功", res)
}

// @Summary 获取当前登录用户详情
// @Description 获取当前登录用户详情
// @Tags 个人中心
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/user/profile [post]
// @Security
func (c *User) Profile(r *ghttp.Request) {
	//获取用户信息
	userInfo, err := sys_user_service.GetCurrentUserInfo(r)

	if err != nil {
		g.Log().Println(err.Error())
		response.FailJson(true, r, err.Error())
	}

	delete(userInfo, "user_password")
	response.SusJson(true, r, "ok", userInfo)
}

// @Summary 修改用户信息
// @Description 修改用户信息
// @Tags 个人中心
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/user/edit [post]
// @Security
func (c *User) Edit(r *ghttp.Request) {

	if r.Method == "POST" {

		var req *sys_user_service.EditParams
		if err := r.Parse(&req); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}

		if _, err := sys_user_service.Edit(req); err != nil {
			response.FailJson(true, r, err.Error())
		}
		response.SusJson(true, r, "修改成功")
	}

}

// @Summary 修改密码
// @Description 修改密码
// @Tags 个人中心
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/user/updatePwd [post]
// @Security
func (c *User) UpdatePwd(r *ghttp.Request) {
	if r.Method == "POST" {
		var req *sys_user_service.UpdatePwdReq
		if err := r.Parse(&req); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}
		if err := sys_user_service.UpdatePwd(r, req); err != nil {
			response.FailJson(true, r, err.Error())
		} else {
			response.SusJson(true, r, "修改成功")
		}

	}
}

// @Summary 添加用户
// @Description 添加用户
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body user.AddUserReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/addUser [post]
// @Security Bearer
func (c *User) AddUser(r *ghttp.Request) {
	if r.Method == "POST" {
		var req *sys_user.AddUserReq
		if err := r.Parse(&req); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}

		InsertId, err := sys_user_service.AddUser(req)
		if err != nil {
			response.FailJson(true, r, err.Error())
		}
		//设置用户所属角色信息
		err = sys_role_service.AddUserRole(req.RoleIds, InsertId)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "设置用户权限失败")
		}
		//设置用户岗位
		err = sys_user_post_service.AddUserPost(req.PostIds, InsertId)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "设置用户岗位信息失败")
		}
		response.SusJson(true, r, "添加管理员成功")
	}
	//获取角色信息
	roleListEntities, err := sys_role_service.GetRoleList()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取角色数据失败")
	}
	//获取岗位信息
	posts, err := sys_post_service.GetUsedPost()
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	res := g.Map{
		"roleList": roleListEntities,
		"posts":    posts,
	}
	response.SusJson(true, r, "成功", res)
}

// @Summary 编辑用户
// @Description 编辑用户
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body user.EditUserReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/editUser [post]
// @Security Bearer
func (c *User) EditUser(r *ghttp.Request) {
	if r.Method == "POST" {
		var req *sys_user.EditUserReq
		if err := r.Parse(&req); err != nil {
			response.FailJson(true, r, err.(*gvalid.Error).FirstString())
		}
		err := sys_user_service.EditUser(req)
		if err != nil {
			response.FailJson(true, r, err.Error())
		}
		//设置用户所属角色信息
		err = sys_role_service.EditUserRole(req.RoleIds, req.UserId)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "设置用户权限失败")
		}
		//设置用户岗位数据
		err = sys_user_post_service.AddUserPost(req.PostIds, gconv.Int64(req.UserId))
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "设置用户岗位信息失败")
		}
		response.SusJson(true, r, "修改管理员成功")
	}
	id := r.GetUint64("id")
	//用户用户信息
	userEntity, err := sys_user.Model.Where("id=?", id).One()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取用户数据失败")
	}
	//获取角色信息
	roleListEntities, err := sys_role_service.GetRoleList()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取角色数据失败")
	}

	//获取已选择的角色信息
	checkedRoleIds, err := sys_user_service.GetAdminRoleIds(id)
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取用户角色数据失败")
	}
	if checkedRoleIds == nil {
		checkedRoleIds = []uint{}
	}
	//获取岗位信息
	posts, err := sys_post_service.GetUsedPost()
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	checkedPosts, err := sys_user_service.GetAdminPosts(id)
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	if checkedPosts == nil {
		checkedPosts = []int64{}
	}
	res := g.Map{
		"roleList":       roleListEntities,
		"userInfo":       userEntity,
		"checkedRoleIds": checkedRoleIds,
		"posts":          posts,
		"checkedPosts":   checkedPosts,
	}
	response.SusJson(true, r, "成功", res)
}

// @Summary 用户列表
// @Description 分页列表
// @Tags 用户管理
// @Param data body user.SearchReq true "data"
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/Auth/userList [get]
// @Security
func (c *User) UserList(r *ghttp.Request) {
	var req *sys_user.SearchReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	total, page, userList, err := sys_user_service.GetAdminList(req)
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取用户列表数据失败")
	}
	users := make([]g.Map, len(userList))
	//获取所有角色信息
	allRoles, err := sys_role_service.GetRoleList()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取用户角色数据失败")
	}
	//获取所有部门信息
	depts, err := sys_dept_service.GetList(&sys_dept.SearchParams{})
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取部门数据失败")
	}
	for k, u := range userList {
		var dept *sys_dept.Dept
		users[k] = gconv.Map(u)
		for _, d := range depts {
			if u.DeptId == d.DeptID {
				dept = d
			}
		}
		users[k]["dept"] = dept
		roles, err := sys_user_service.GetAdminRole(u.Id, allRoles)
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "获取用户角色数据失败")
		}
		roleInfo := make([]g.Map, 0, len(roles))
		for _, r := range roles {
			roleInfo = append(roleInfo, g.Map{"roleId": r.Id, "name": r.Name})
		}
		users[k]["user_status"] = gconv.String(u.UserStatus)
		users[k]["roleInfo"] = roleInfo
	}
	//用户状态
	statusOptions, err := sys_dict_type_service.GetDictWithDataByType("sys_normal_disable", "", "")
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	//用户性别
	userGender, err := sys_dict_type_service.GetDictWithDataByType("sys_user_sex", "", "")
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	res := g.Map{
		"total":         total,
		"currentPage":   page,
		"userList":      users,
		"statusOptions": statusOptions,
		"userGender":    userGender,
	}
	response.SusJson(true, r, "成功", res)
}

// @Summary 删除管理员
// @Description 删除管理员
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param ids path integer  true "ids[1,2,3...]"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/deleteAdmin [delete]
// @Security Bearer
func (c *User) DeleteAdmin(r *ghttp.Request) {
	ids := r.GetInts("ids")
	if len(ids) > 0 {
		_, err := sys_user.Model.Where("id in(?)", ids).Delete()
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "删除失败")
		}
	} else {
		response.FailJson(true, r, "删除失败，参数错误")
	}
	//删除对应权限
	enforcer, err := casbin_adapter_service.GetEnforcer()
	if err == nil {
		for _, v := range ids {
			enforcer.RemoveFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", v))
		}
	}
	//删除用户对应的岗位
	_, err = sys_user_post.Delete(sys_user_post.Columns.UserId+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
	}
	response.SusJson(true, r, "删除成功")
}

// @Summary 设置角色状态
// @Description 设置角色状态
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body role.StatusSetReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/statusSetRole [post]
// @Security Bearer
func (c *User) StatusSetRole(r *ghttp.Request) {
	var req *sys_role.StatusSetReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	err := sys_role_service.StatusSetRole(req)
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	response.SusJson(true, r, "状态设置成功")
}


// @Summary 修改用户状态
// @Description 修改用户状态
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body user.StatusReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/changeUserStatus [post]
// @Security Bearer
func (c *User) ChangeUserStatus(r *ghttp.Request) {
	var req *sys_user.StatusReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	if err := sys_user_service.ChangeUserStatus(req); err != nil {
		response.FailJson(true, r, err.Error())
	} else {
		response.SusJson(true, r, "用户状态设置成功")
	}
}

// @Summary 重置用户密码
// @Description 重置用户密码
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body user.ResetPwdReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/resetUserPwd [post]
// @Security Bearer
func (c *User) ResetUserPwd(r *ghttp.Request) {
	var req *sys_user.ResetPwdReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	if err := sys_user_service.ResetUserPwd(req); err != nil {
		response.FailJson(true, r, err.Error())
	} else {
		response.SusJson(true, r, "用户密码重置成功")
	}
}

