package sys_role_controller

import (
	"fmt"
	"gf-admin/app/pojo/sys/sys_role"
	"gf-admin/app/service/cache_service"
	"gf-admin/app/service/casbin_adapter_service"
	"gf-admin/app/service/sys/sys_role_service"
	"gf-admin/app/service/sys/sys_menu_service"
	"gf-admin/app/service/sys/sys_dict_type_service"
	"gf-admin/library/response"
	"gf-admin/library/utils"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/text/gstr"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/gvalid"
)

//菜单用户组用户管理
type Auth struct{}

// @Summary 角色列表
// @Description 分页列表
// @Tags 角色管理
// @Param data body role.SelectPageReq true "data"
// @Success 0 {object} response.Response "{"code": 200, "data": [...]}"
// @Router /system/auth/roleList [get]
// @Security
func (c *Auth) RoleList(r *ghttp.Request) {
	var req *sys_role.SelectPageReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	//获取角色列表
	total, page, list, err := sys_role_service.GetRoleListSearch(req)
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取数据失败")
	}
	//菜单正常or停用状态
	statusOptions, err := sys_dict_type_service.GetDictWithDataByType("sys_normal_disable", "", "")
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	response.SusJson(true, r, "成功", g.Map{
		"currentPage": page,
		"total":       total,
		"list":        list,
		"searchTypes": statusOptions,
	})
}

// @Summary 添加角色
// @Description 添加角色
// @Tags 角色管理
// @Accept  application/json
// @Product application/json
// @Param data body string  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/addRole [post]
// @Security Bearer
func (c *Auth) AddRole(r *ghttp.Request) {
	//添加操作
	if r.Method == "POST" {
		//获取表单提交的数据
		res := r.GetFormMap()

		tx, err := g.DB("default").Begin() //开启事务
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "事务处理失败")
		}
		//插入角色
		//添加角色获取添加的id
		insertId, err := sys_role_service.AddRole(tx, res)
		if err != nil {
			tx.Rollback() //回滚
			response.FailJson(true, r, err.Error())
		}
		//添加角色权限
		err = sys_role_service.AddRoleRule(res["menuIds"], insertId)
		if err != nil {
			tx.Rollback() //回滚
			g.Log().Error(err.Error())
			response.FailJson(true, r, "添加角色失败")
		}
		tx.Commit()
		//清除TAG缓存
		cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
		response.SusJson(true, r, "添加角色成功")
	}

	//获取菜单信息
	mListEntities, err := sys_menu_service.GetMenuList()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取菜单数据失败")
	}
	var mList g.ListStrAny
	for _, entity := range mListEntities {
		m := g.Map{
			"id":    entity.Id,
			"pid":   entity.Pid,
			"label": entity.Title,
		}
		mList = append(mList, m)
	}
	mList = utils.PushSonToParent(mList)
	res := g.Map{
		"menuList": mList,
	}
	response.SusJson(true, r, "成功", res)
}

// @Summary 修改角色
// @Description 修改角色
// @Tags 角色管理
// @Accept  application/json
// @Product application/json
// @Param data body string  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/editRole [post]
// @Security Bearer
func (c *Auth) EditRole(r *ghttp.Request) {
	id := r.GetRequestInt64("roleId")
	if r.Method == "POST" {
		//获取表单提交的数据
		res := r.GetFormMap()
		tx, err := g.DB("default").Begin() //开启事务
		if err != nil {
			g.Log().Error(err)
			response.FailJson(true, r, "事务处理失败")
		}
		//修改角色信息
		err = sys_role_service.EditRole(tx, res)
		if err != nil {
			tx.Rollback() //回滚
			response.FailJson(true, r, err.Error())
		}
		//添加角色权限
		err = sys_role_service.EditRoleRule(res["menuIds"], id)
		if err != nil {
			tx.Rollback() //回滚
			g.Log().Error(err.Error())
			response.FailJson(true, r, "添加用户组失败")
		}
		tx.Commit()
		//清除TAG缓存
		cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
		response.SusJson(true, r, "修改用户组成功")
	}
	//获取角色信息
	role, err := sys_role.Model.Where("id=?", id).One()
	if err != nil {
		response.FailJson(true, r, "获取角色数据失败")
	}

	//获取菜单信息
	mListEntities, err := sys_menu_service.GetMenuList()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取菜单数据失败")
	}
	//获取角色关联的菜单规则
	enforcer, err := casbin_adapter_service.GetEnforcer()
	if err != nil {
		g.Log().Error(err)
		response.FailJson(true, r, "获取权限处理器失败")
	}
	gp := enforcer.GetFilteredNamedPolicy("p", 0, fmt.Sprintf("g_%d", id))
	gpSlice := make([]int, len(gp))
	for k, v := range gp {
		gpSlice[k] = gconv.Int(gstr.SubStr(v[1], 2))
	}

	var mList g.ListStrAny
	for _, entity := range mListEntities {
		m := g.Map{
			"id":    entity.Id,
			"pid":   entity.Pid,
			"label": entity.Title,
		}
		mList = append(mList, m)
	}

	mList = utils.PushSonToParent(mList)
	res := g.Map{
		"menuList":     mList,
		"role":         role,
		"checkedRules": gpSlice,
	}
	response.SusJson(true, r, "成功", res)
}

// @Summary 删除角色
// @Description 删除角色
// @Tags 角色管理
// @Accept  application/json
// @Product application/json
// @Param ids body integer  true "ids[1,2,3]"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/deleteRole [delete]
// @Security Bearer
func (c *Auth) DeleteRole(r *ghttp.Request) {
	ids := r.GetInts("ids")
	if len(ids) == 0 {
		response.FailJson(true, r, "删除失败，参数错误")
	}
	err := sys_role_service.DeleteRoleByIds(ids)
	if err != nil {
		response.FailJson(true, r, "删除失败，"+err.Error())
	}
	//清除TAG缓存
	cache_service.New().RemoveByTag(cache_service.AdminAuthTag)
	response.SusJson(true, r, "删除成功")
}

// @Summary 角色数据权限分配
// @Description 角色数据权限分配
// @Tags 用户管理
// @Accept  application/json
// @Product application/json
// @Param data body role.DataScopeReq  true "data"
// @Success 200 {object} response.Response	"{"code": 0, "message": "添加成功"}"
// @Router /system/auth/roleDataScope [post]
// @Security Bearer
func (c *Auth) RoleDataScope(r *ghttp.Request) {
	var req *sys_role.DataScopeReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		response.FailJson(true, r, err.(*gvalid.Error).FirstString())
	}
	err := sys_role_service.RoleDataScope(req)
	if err != nil {
		response.FailJson(true, r, err.Error())
	}
	response.SusJson(true, r, "数据权限设置成功", req)
}
