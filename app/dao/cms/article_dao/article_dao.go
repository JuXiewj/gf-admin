package article_dao

import (
	"gf-admin/app/pojo/cms/article"
	"github.com/gogf/gf/errors/gerror"
    "github.com/gogf/gf/frame/g"
)


// GetByID 根据ID查询记录
func GetByID(id int64) (*article.Entity, error) {
	entity, err := article.Model.FindOne(id)
	if err != nil {
		g.Log().Error(err)
		return nil, gerror.New("根据ID查询记录出错")
	}
	if entity == nil {
		return nil, gerror.New("根据ID未能查询到记录")
	}
	return entity, nil
}


