package gen_table_dao

import (
	"gf-admin/app/dao/gen/gen_table_column_dao"
	"gf-admin/app/pojo/gen/gen_table"
	"gf-admin/app/pojo/gen/gen_table_column"
	"gf-admin/library/service"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/text/gstr"
	"github.com/gogf/gf/util/gconv"
)


//查询据库列表
func SelectDbTableList(param *gen_table.SelectPageReq) (total int, list []*gen_table.Entity, err error) {
	db := g.DB()
	sql := " from information_schema.tables where table_schema = (select database())" +
		" and table_name NOT LIKE 'qrtz_%' AND table_name NOT LIKE 'gen_%' and table_name NOT IN (select table_name from gen_table) "
	if param != nil {
		if param.TableName != "" {
			sql += gdb.FormatSqlWithArgs(" and lower(table_name) like lower(?)", []interface{}{"%" + param.TableName + "%"})
		}

		if param.TableComment != "" {
			sql += gdb.FormatSqlWithArgs(" and lower(table_comment) like lower(?)", []interface{}{"%" + param.TableComment + "%"})
		}

		if param.BeginTime != "" {
			sql += gdb.FormatSqlWithArgs(" and date_format(create_time,'%y%m%d') >= date_format(?,'%y%m%d') ", []interface{}{param.BeginTime})
		}

		if param.EndTime != "" {
			sql += gdb.FormatSqlWithArgs(" and date_format(create_time,'%y%m%d') <= date_format(?,'%y%m%d') ", []interface{}{param.EndTime})
		}
	}
	countSql := "select count(1) " + sql
	total, err = db.GetCount(countSql)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("读取总表数失败")
		return
	}
	sql = "table_name, table_comment, create_time, update_time " + sql

	if param.PageNum == 0 {
		param.PageNum = 1
	}

	if param.PageSize == 0 {
		param.PageSize = service.AdminPageNum
	}
	page := (param.PageNum - 1) * param.PageSize
	sql += " order by create_time desc,table_name asc limit  " + gconv.String(page) + "," + gconv.String(param.PageSize)
	var res gdb.Result
	res, err = db.GetAll("select " + sql)
	res.Structs(&list)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("读取数据失败")
	}
	return
}

//根据条件分页查询数据
func SelectListByPage(param *gen_table.SelectPageReq) (total int, list []*gen_table.Entity, err error) {
	model := gen_table.Model
	if param != nil {
		if param.TableName != "" {
			model = model.Where(gen_table.Columns.TableName+" like ?", "%"+param.TableName+"%")
		}
		if param.TableComment != "" {
			model = model.Where(gen_table.Columns.TableComment+"like ?", "%"+param.TableComment+"%")
		}
		if param.BeginTime != "" {
			model = model.Where(gen_table.Columns.CreateTime+" >= ", param.BeginTime)
		}
		if param.EndTime != "" {
			model = model.Where(gen_table.Columns.CreateTime+" <= ", param.EndTime)
		}
		total, err = model.Count()
		if err != nil {
			g.Log().Error(err)
			err = gerror.New("获取总行数失败")
			return
		}
		if param.PageNum == 0 {
			param.PageNum = 1
		}
		if param.PageSize == 0 {
			param.PageSize = service.AdminPageNum
		}
		list, err = model.Page(param.PageNum, param.PageSize).Order(gen_table.Columns.TableId + " asc").All()
		if err != nil {
			g.Log().Error(err)
			err = gerror.New("获取数据失败")
			return
		}
	}
	return
}

//查询据库列表
func SelectDbTableListByNames(tableNames []string) ([]*gen_table.Entity, error) {
	db := g.DB()
	sql := "select * from information_schema.tables where table_name NOT LIKE 'qrtz_%' and table_name NOT LIKE 'gen_%' " +
		" and table_schema = (select database()) "
	if len(tableNames) > 0 {
		in := gstr.TrimRight(gstr.Repeat("?,", len(tableNames)), ",")
		sql += " and " + gdb.FormatSqlWithArgs("table_name in ("+in+")", gconv.SliceAny(tableNames))
	}
	var result []*gen_table.Entity
	res, err := db.GetAll(sql)
	if err != nil {
		g.Log().Error(err)
		return nil, gerror.New("获取表格信息失败")
	}
	err = res.Structs(&result)
	if err != nil {
		g.Log().Error(err)
		return nil, gerror.New("表格信息转换失败")
	}
	return result, err
}

//通过表格ID获取表格信息
func GetInfoById(tableId int64) (entity *gen_table.Entity, err error) {
	entity, err = gen_table.Model.FindOne(tableId)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取表格信息出错")
	}
	return
}

func SelectRecordById(tableId int64) (entityExtend *gen_table.EntityExtend, err error) {
	var entity *gen_table.Entity
	entity, err = GetInfoById(tableId)
	if err != nil {
		return
	}
	m := gconv.Map(entity)
	gconv.Struct(m, &entityExtend)

	//表字段数据
	var columns []*gen_table_column.Entity
	columns, err = gen_table_column_dao.SelectGenTableColumnListByTableId(tableId)
	if err != nil {
		return
	}
	entityExtend.Columns = columns
	return
}
