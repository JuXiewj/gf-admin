package sys_user_post_dao

import (
	"gf-admin/app/pojo/sys/sys_post"
	"gf-admin/app/pojo/sys/sys_user_post"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)

//删除对应用户的岗位信息
func DeleteByUserId(userId int64) error {
	_, err := sys_user_post.Model.Delete(sys_user_post.Columns.UserId, userId)
	return err
}

//添加用户岗位
func AddUserPost(postIds []int64, userId int64) (err error) {
	data := g.List{}
	for _, v := range postIds {
		data = append(data, g.Map{
			sys_user_post.Columns.UserId: userId,
			sys_user_post.Columns.PostId: v,
		})
	}
	_, err = sys_user_post.Model.Data(data).Insert()
	return
}

//获取用户岗位
func GetAdminPosts(userId uint64) (postIds []int64, err error) {
	list, e := sys_user_post.Model.All(sys_user_post.Columns.UserId, userId)
	if e != nil {
		g.Log().Error(e)
		err = gerror.New("获取用户岗位信息失败")
		return
	}
	for _, entity := range list {
		postIds = append(postIds, entity.PostId)
	}
	return
}

//根据用户id获取岗位信息详情
func GetPostsByUserId(userId uint64) ([]*sys_post.Entity, error) {
	model := g.DB().Table(sys_user_post.Table)
	datas := ([]*sys_post.Entity)(nil)
	err := model.As("a").InnerJoin("sys_post b", "a.post_id = b.post_id").Fields("b.*").Where(sys_user_post.Columns.UserId, userId).Structs(&datas)

	if err != nil {
		return nil, err
	}

	return datas, nil
}
