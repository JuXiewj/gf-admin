package sys_oper_log_dao

import (
	"gf-admin/app/pojo/sys/sys_menu"
	"gf-admin/app/pojo/sys/sys_oper_log"
	"gf-admin/app/pojo/sys/sys_user"
	"gf-admin/library/service"
	"gf-admin/library/utils"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
	"net/url"
)

// Fill with you ideas below.



//新增操作日志记录
func Add(user *sys_user.Entity, menu *sys_menu.Entity, url *url.URL,
	param g.Map, method, clientIp string) {
	var operLog sys_oper_log.Entity
	if menu != nil {
		operLog.Title = menu.Title
	}
	operLog.Method = url.Path
	operLog.RequestMethod = method
	operLog.OperatorType = 1
	operLog.OperName = user.UserName
	rawQuery := url.RawQuery
	if rawQuery != "" {
		rawQuery = "?" + rawQuery
	}
	operLog.OperUrl = url.Path + rawQuery
	operLog.OperIp = clientIp
	operLog.OperLocation = utils.GetCityByIp(operLog.OperIp)
	operLog.OperTime = gtime.Timestamp()
	if param != nil {
		if v, ok := param["apiReturnRes"]; ok {
			res := gconv.Map(v)
			if gconv.Int(res["code"]) == 0 {
				operLog.Status = 1
			} else {
				operLog.Status = 0
			}
			if _, ok = res["data"]; ok {
				delete(res, "data")
			}
			b, _ := gjson.Encode(res)
			if len(b) > 0 {
				operLog.JsonResult = string(b)
			}
			delete(param, "apiReturnRes")
		}
		b, _ := gjson.Encode(param)
		if len(b) > 0 {
			operLog.OperParam = string(b)
		}
	}
	sys_oper_log.Model.Save(operLog)
}

//操作日志列表
func ListByPage(req *sys_oper_log.SelectPageReq) (total, page int, list []*sys_oper_log.Entity, err error) {
	model := sys_oper_log.Model
	order := "oper_id DESC"
	if req != nil {
		if req.OperName != "" {
			model = model.Where("oper_name like ?", "%"+req.OperName+"%")
		}
		if req.Title != "" {
			model = model.Where("title like ?", "%"+req.Title+"%")
		}
		if req.Status != "" {
			model = model.Where("status", gconv.Int(req.Status))
		}
		if req.BeginTime != "" {
			model = model.Where("oper_time >=", utils.StrToTimestamp(req.BeginTime))
		}
		if req.EndTime != "" {
			model = model.Where("oper_time <=", utils.StrToTimestamp(req.EndTime))
		}
		if req.SortName != "" {
			if req.SortOrder != "" {
				order = req.SortName + " " + req.SortOrder
			} else {
				order = req.SortName + " DESC"
			}
		}
	}
	total, err = model.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}
	if req.PageNum == 0 {
		req.PageNum = 1
	}
	page = req.PageNum
	if req.PageSize == 0 {
		req.PageSize = service.AdminPageNum
	}
	list, err = model.Page(page, req.PageSize).Order(order).All()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
		return
	}
	return
}

//通过id获取操作日志
func GetById(id int64) (log *sys_oper_log.Entity, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	log, err = sys_oper_log.Model.FindOne("oper_id", id)
	if err != nil {
		g.Log().Error(err)
	}
	if err != nil || log == nil {
		err = gerror.New("获取操作日志失败")
	}
	return
}

//删除
func DeleteByIds(ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}
	_, err = sys_oper_log.Model.Delete("oper_id in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}

//清空
func ClearLog() (err error) {
	_, err = g.DB().Exec("truncate " + sys_oper_log.Table)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("清除失败")
	}
	return
}
