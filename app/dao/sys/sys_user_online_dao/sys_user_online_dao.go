package user_online

import (
	"gf-admin/app/pojo/sys/sys_user_online"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)


//获取在线用户列表
func GetOnlineListPage(req *sys_user_online.ReqListSearch, hasToken bool) (total, page int, list []*sys_user_online.Entity, err error) {
	page = req.PageNum
	model := sys_user_online.Model
	if req.Ip != "" {
		model = model.Where("ip like ?", "%"+req.Ip+"%")
	}
	if req.Username != "" {
		model = model.Where("user_name like ?", "%"+req.Username+"%")
	}
	total, err = model.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}
	if !hasToken {
		list, err = model.FieldsEx("token").Page(page, req.PageSize).Order("create_time DESC").All()
	} else {
		list, err = model.Page(page, req.PageSize).Order("create_time DESC").All()
	}
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
		return
	}
	return
}

func GetInfoById(id int) (entity *sys_user_online.Entity, err error) {
	entity, err = sys_user_online.Model.FindOne("id", id)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取在线用户信息失败")
	}
	return
}
