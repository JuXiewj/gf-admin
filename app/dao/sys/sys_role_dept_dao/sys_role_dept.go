package sys_role_dept_dao

import "gf-admin/app/pojo/sys/sys_role_dept"

// Fill with you ideas below.

//获取角色的部门数据
func GetRoleDepts(roleId int64) ([]int64, error) {
	entity, err := sys_role_dept.Model.All( sys_role_dept.Columns.RoleId, roleId)
	if err != nil {
		return nil, err
	}
	d := make([]int64, len(entity))
	for k, v := range entity {
		d[k] = v.DeptId
	}
	return d, nil
}
