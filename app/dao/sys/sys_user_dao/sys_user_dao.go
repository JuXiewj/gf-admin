package sys_user_dao

import (
	"gf-admin/app/pojo/sys/sys_user"
	"gf-admin/library/utils"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
)

func GetUserById(id uint64) (*sys_user.Entity, error) {
	return sys_user.Model.Where("id", id).One()
}

//添加管理员操作
func Add(req *sys_user.AddUserReq) (InsertId int64, err error) {
	if i, _ := sys_user.Model.Where("user_name=?", req.UserName).Count(); i != 0 {
		err = gerror.New("用户名已经存在")
		return
	}
	if i, _ := sys_user.Model.Where("mobile=?", req.Phonenumber).Count(); i != 0 {
		err = gerror.New("手机号已经存在")
		return
	}
	//保存管理员信息
	entity := new(sys_user.Entity)
	entity.UserName = req.UserName
	entity.DeptId = req.DeptId
	entity.UserStatus = req.Status
	entity.CreateTime = gconv.Int(gtime.Timestamp())
	entity.Mobile = req.Phonenumber
	entity.Sex = req.Sex
	entity.UserEmail = req.Email
	entity.UserNickname = req.NickName
	entity.UserPassword = req.Password
	entity.Remark = req.Remark
	entity.IsAdmin = req.IsAdmin
	res, err := sys_user.Model.Save(entity)
	if err != nil {
		return
	}
	InsertId, _ = res.LastInsertId()
	return
}

//修改用户信息
func Edit(req *sys_user.EditUserReq) (err error) {
	if i, _ := sys_user.Model.Where("id!=? and mobile=?", req.UserId, req.Phonenumber).Count(); i != 0 {
		err = gerror.New("手机号已经存在")
		return
	}
	//保存管理员信息
	var entity *sys_user.Entity
	entity, err = sys_user.Model.Where("id", req.UserId).One()
	if err != nil || entity == nil {
		g.Log().Error(err)
		err = gerror.New("获取用户信息失败")
		return
	}
	entity.DeptId = req.DeptId
	entity.UserStatus = req.Status
	entity.Mobile = req.Phonenumber
	entity.Sex = req.Sex
	entity.UserEmail = req.Email
	entity.UserNickname = req.NickName
	entity.Remark = req.Remark
	entity.IsAdmin = req.IsAdmin
	_, err = sys_user.Model.Save(entity)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("修改用户信息失败")
	}
	return
}

//获取管理员列表
func GetAdminList(req *sys_user.SearchReq) (total, page int, userList []*sys_user.Entity, err error) {
	userModel := sys_user.Model
	if req != nil {
		if req.KeyWords != "" {
			keyWords := "%" + req.KeyWords + "%"
			userModel = userModel.Where("user_name like ? or  user_nickname like ?",
				keyWords, keyWords)
		}
		if len(req.DeptIds) != 0 {
			userModel = userModel.Where("dept_id in (?)", req.DeptIds)
		}
		if req.Status != "" {
			userModel = userModel.Where("user_status", gconv.Int(req.Status))
		}
		if req.Phonenumber != "" {
			userModel = userModel.Where("mobile like ?", "%"+req.Phonenumber+"%")
		}
		if req.BeginTime != "" {
			userModel = userModel.Where("create_time >=?", utils.StrToTimestamp(req.BeginTime))
		}
		if req.EndTime != "" {
			userModel = userModel.Where("create_time <=?", utils.StrToTimestamp(req.EndTime))
		}
	}
	total, err = userModel.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}
	if req.PageNum == 0 {
		req.PageNum = 1
	}
	page = req.PageNum
	userList, err = userModel.Page(page, req.PageSize).Order("id asc").All()
	return
}

//修改用户状态
func ChangeUserStatus(req *sys_user.StatusReq) error {
	user, err := sys_user.Model.Where("id", req.Id).One()
	if err != nil || user == nil {
		g.Log().Error(err)
		return gerror.New("用户不存在")
	}
	user.UserStatus = req.UserStatus
	_, err = sys_user.Model.Save(user)
	if err != nil {
		g.Log().Error(err)
		return gerror.New("修改用户状态失败")
	}
	return nil
}

//重置用户密码
func ResetUserPwd(req *sys_user.ResetPwdReq) error {
	user, err := sys_user.Model.Where("id", req.Id).One()
	if err != nil || user == nil {
		g.Log().Error(err)
		return gerror.New("用户不存在")
	}
	user.UserPassword = req.Password
	_, err = sys_user.Model.Save(user)
	if err != nil {
		g.Log().Error(err)
		return gerror.New("修改用户密码失败")
	}
	return nil
}
