package sys_role_dao

import (
	"database/sql"
	"errors"
	"fmt"
	"gf-admin/app/pojo/sys/sys_role"
	"gf-admin/app/pojo/sys/sys_role_dept"
	"gf-admin/app/service/cache_service"
	"gf-admin/app/service/casbin_adapter_service"
	"gf-admin/library/service"
	"gf-admin/library/utils"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/gf/util/gvalid"
)


func GetRoleListSearch(req *sys_role.SelectPageReq) (total, page int, list []*sys_role.Entity, err error) {
	model := sys_role.Model
	if req != nil {
		if req.RoleName != "" {
			model = model.Where("name like ?", "%"+req.RoleName+"%")
		}
		if req.Status != "" {
			model = model.Where("status", gconv.Int(req.Status))
		}
		if req.BeginTime != "" {
			model = model.Where("create_time >= ? ", utils.StrToTimestamp(req.BeginTime))
		}

		if req.EndTime != "" {
			model = model.Where("create_time<=?", utils.StrToTimestamp(req.EndTime))
		}
	}
	total, err = model.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}
	if req.PageNum == 0 {
		req.PageNum = 1
	}
	page = req.PageNum
	if req.PageSize == 0 {
		req.PageSize = service.AdminPageNum
	}

	list, err = model.Page(page, req.PageSize).Order("id asc").All()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
		return
	}
	return
}

//获取用户组(角色)列表
func GetList() (list []*sys_role.Entity, err error) {
	cache := cache_service.New()
	//从缓存获取
	iList := cache.Get(cache_service.AdminAuthRole)
	if iList != nil {
		list = iList.([]*sys_role.Entity)
		return
	}
	//从数据库获取
	list, err = sys_role.Model.Order("list_order asc,id asc").All()
	//缓存数据
	cache.Set(cache_service.AdminAuthRole, list, 0, cache_service.AdminAuthTag)
	return
}

func checkRoleData(params map[string]interface{}) error {
	rules := []string{
		"roleName@required|length:1,20#请填写角色名称|名称应在:min到:max个字符之间",
	}

	e := gvalid.CheckMap(params, rules)
	if e != nil {
		return e
	}
	return nil
}

//保存角色信息并返回插入的id
func Add(tx *gdb.TX, data map[string]interface{}) (InsId int64, err error) {
	if e := checkRoleData(data); e != nil {
		err = gerror.New(e.(*gvalid.Error).FirstString())
		return
	}
	//保存角色信息
	now := gtime.Timestamp()
	roleMap := gdb.Map{
		"status":      data["status"],
		"name":        data["roleName"],
		"create_time": now,
		"update_time": now,
		"list_order":  data["roleSort"],
		"remark":      data["remark"],
	}
	var res sql.Result
	res, err = tx.Table(sys_role.Table).Data(roleMap).Save()
	if err != nil {
		return
	}
	InsId, _ = res.LastInsertId()
	return
}

//修改角色信息操作
func Edit(tx *gdb.TX, data map[string]interface{}) (err error) {
	if _, k := data["roleId"]; !k {
		err = errors.New("缺少更新条件Id")
		return
	}
	if e := checkRoleData(data); e != nil {
		err = gerror.New(e.(*gvalid.Error).FirstString())
		return
	}
	//保存角色信息
	now := gtime.Timestamp()
	roleMap := gdb.Map{
		"id":          data["roleId"],
		"status":      data["status"],
		"name":        data["roleName"],
		"update_time": now,
		"list_order":  data["roleSort"],
		"remark":      data["remark"],
	}
	_, err = tx.Table(sys_role.Table).Data(roleMap).Save()
	if err != nil {
		return
	}
	return
}

//设置角色状态
func StatusSetRole(req *sys_role.StatusSetReq) error {
	if req != nil {
		entity, err := sys_role.Model.Where("id", req.RoleId).One()
		if err != nil {
			g.Log().Error(err)
			return gerror.New("获取角色信息失败")
		}
		entity.Status = req.Status
		_, err = sys_role.Model.Save(entity)
		if err != nil {
			g.Log().Error(err)
			return gerror.New("设置状态失败")
		}
	}
	return nil
}

//设置角色数据权限
func DataScope(req *sys_role.DataScopeReq) error {
	tx, err := g.DB().Begin()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("设置失败")
		return err
	}
	_, err = tx.Table(sys_role.Table).Where("id", req.RoleId).Data(g.Map{"data_scope": req.DataScope}).Update()
	if err != nil {
		g.Log().Error(err)
		tx.Rollback()
		return gerror.New("设置失败")
	}
	if req.DataScope == 2 {
		_, err := tx.Table(sys_role_dept.Table).Delete(sys_role_dept.Columns.RoleId, req.RoleId)
		if err != nil {
			g.Log().Error(err)
			tx.Rollback()
			return gerror.New("设置失败")
		}
		//自定义数据权限
		data := g.List{}
		for _, deptId := range req.DeptIds {
			data = append(data, g.Map{sys_role_dept.Columns.RoleId: req.RoleId, sys_role_dept.Columns.DeptId: deptId})
		}
		_, err = tx.Table(sys_role_dept.Table).Data(data).Insert()
		if err != nil {
			g.Log().Error(err)
			tx.Rollback()
			return gerror.New("设置失败")
		}
	}
	tx.Commit()
	return nil
}

//删除角色权限操作
func DeleteRoleRule(roleId int) (err error) {
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	//查询当前权限
	gp := enforcer.GetFilteredNamedPolicy("p", 0, fmt.Sprintf("g_%d", roleId))
	//删除旧权限
	for _, v := range gp {
		_, e = enforcer.RemovePolicy(v)
		if e != nil {
			err = e
			return
		}
	}
	return
}

func DeleteByIds(ids []int) (err error) {
	//查询所有子级id
	roleAllEntity, err := GetList()
	if err != nil {
		g.Log().Debug(err)
		err = gerror.New("删除失败，不存在角色信息")
		return
	}
	roleAll := gconv.SliceMap(roleAllEntity)
	sonList := make(g.List, 0, len(roleAll))
	for _, id := range ids {
		sonList = append(sonList, utils.FindSonByParentId(roleAll, id, "parent_id", "id")...)
	}
	for _, role := range sonList {
		ids = append(ids, gconv.Int(role["id"]))
	}
	tx, err := g.DB("default").Begin() //开启事务
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("事务处理失败")
		return
	}
	_, err = tx.Table(sys_role.Table).Where("id in(?)", ids).Delete()
	if err != nil {
		g.Log().Error(err)
		tx.Rollback()
		err = gerror.New("删除失败")
		return
	}
	//删除角色的权限和管理的部门数据权限
	for _, v := range ids {
		err = DeleteRoleRule(v)
		if err != nil {
			g.Log().Error(err)
			tx.Rollback()
			err = gerror.New("删除失败")
			return
		}
		_, err = tx.Table(sys_role_dept.Table).Delete(sys_role_dept.Columns.RoleId, v)
		if err != nil {
			g.Log().Error(err)
			tx.Rollback()
			err = gerror.New("删除失败")
			return
		}
	}
	tx.Commit()
	return
}
