// ==========================================================================
// 生成日期：2021-01-08 16:09:25
// 生成人：gfast
// ==========================================================================
package article

// AddReq 用于存储新增请求的请求参数
type AddReq struct {
	 Name  string   `p:"name" v:"required#name不能为空"`
	 Context  string   `p:"context" `
	 Remark  string   `p:"remark" `
}
// EditReq 用于存储修改请求参数
type EditReq struct {
	Id    int64  `p:"id" v:"required#主键ID不能为空"`
	Name  string `p:"name" v:"required#name不能为空"`
	Context  string `p:"context" `
	Remark  string `p:"remark" `
}
// RemoveReq 用于存储删除请求参数
type RemoveReq struct {
	Ids []int `p:"ids"` //删除id
}
// SelectPageReq 用于存储分页查询的请求参数
type SelectPageReq struct {
	Name       string `p:"name"` //name
	Context    string `p:"context"` //context
	BeginTime  string `p:"beginTime"`  //开始时间
	EndTime    string `p:"endTime"`    //结束时间
	PageNum    int    `p:"pageNum"`    //当前页码
	PageSize   int    `p:"pageSize"`   //每页数
}


