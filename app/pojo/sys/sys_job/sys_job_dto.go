package sys_job

// Fill with you ideas below.

//添加操作请求参数
type ReqAdd struct {
	JobName        string `p:"job_name" v:"required#任务名称不能为空"`
	JobParams      string `p:"job_params"` // 任务参数
	JobGroup       string `p:"job_group" `
	InvokeTarget   string `p:"invoke_target" v:"required#执行方法不能为空"`
	CronExpression string `p:"cron_expression" v:"required#任务表达式不能为空"`
	MisfirePolicy  int    `p:"misfire_policy"`
	Concurrent     int    `p:"concurrent" `
	Status         int    `p:"status" v:"required#状态（0正常 1暂停）不能为空"`
	Remark         string `p:"remark" `
}

//修改操作请求参数
type ReqEdit struct {
	JobId int64 `p:"job_id" v:"min:1#任务id不能为空"`
	ReqAdd
}

//分页请求参数
type SelectPageReq struct {
	JobName  string `p:"jobName"`  //任务名称
	JobGroup string `p:"jobGroup"` //任务组名
	Status   string `p:"status"`   //状态（0正常 1暂停）
	PageNum  int    `p:"pageNum"`  //当前页码
	PageSize int    `p:"pageSize"` //每页数
}
