package sys_user_online

// Fill with you ideas below.
//列表搜索参数
type ReqListSearch struct {
	Username string `p:"userName"`
	Ip       string `p:"ipaddr"`
	PageNum  int    `p:"page"`     //当前页码
	PageSize int    `p:"pageSize"` //每页数
}
