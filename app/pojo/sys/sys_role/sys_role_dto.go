package sys_role

//分页请求参数
type SelectPageReq struct {
	RoleName  string `p:"roleName"`  //参数名称
	BeginTime string `p:"beginTime"` //开始时间
	EndTime   string `p:"endTime"`   //结束时间
	Status    string `p:"status"`    //状态
	PageNum   int    `p:"pageNum"`   //当前页码
	PageSize  int    `p:"pageSize"`  //每页数
}

//修改状态参数
type StatusSetReq struct {
	RoleId uint `p:"roleId" v:"required#角色ID不能为空"`
	Status uint `p:"status" v:"required#状态不能为空"`
}

//角色数据授权参数
type DataScopeReq struct {
	RoleId    uint   `p:"roleId" v:"required#角色ID不能为空"`
	DataScope uint   `p:"dataScope" v:"required#权限范围不能为空"`
	DeptIds   []uint `p:"deptIds"`
}
