package sys_menu

//菜单对象
type MenuReq struct {
	MenuType   uint   `p:"menuType"  v:"min:0|max:2#菜单类型最小值为:min|菜单类型最大值为:max"`
	Pid        uint   `p:"parentId"  v:"min:0"`
	Name       string `p:"name" v:"required#请填写规则名称"`
	Title      string `p:"menuName"  v:"required|length:1,100#请填写标题|标题长度在:min到:max位"`
	Icon       string `p:"icon"`
	Weigh      int    `p:"orderNum" `
	Condition  string `p:"condition" `
	Remark     string `p:"remark" `
	Status     uint   `p:"status" `
	AlwaysShow uint   `p:"visible"`
	Path       string `p:"path"`
	Component  string `p:"component" v:"required-if:menuType,1#组件路径不能为空"`
	IsFrame    uint   `p:"is_frame"`
}

type ReqSearch struct {
	Status string `p:"status" `
	Title  string `p:"menuName" `
}