package sys_oper_log


//查询列表请求参数
type SelectPageReq struct {
	Title     string `p:"title"`         //系统模块
	OperName  string `p:"operName"`      //操作人员
	Status    string `p:"status"`        //操作状态
	BeginTime string `p:"beginTime"`     //数据范围
	EndTime   string `p:"endTime"`       //开始时间
	PageNum   int    `p:"pageNum"`       //当前页码
	PageSize  int    `p:"pageSize"`      //每页数
	SortName  string `p:"orderByColumn"` //排序字段
	SortOrder string `p:"isAsc"`         //排序方式
}

