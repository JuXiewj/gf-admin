package sys_dept

import (
	"github.com/gogf/gf/os/gtime"
)

//数据列表数据结构
type Dept struct {
	SearchValue interface{}            `json:"searchValue"`
	CreateBy    string                 `json:"createBy"  orm:"create_by"`
	CreateTime  *gtime.Time            `json:"createTime" orm:"create_time"`
	UpdateBy    string                 `json:"updateBy"  orm:"update_by"`
	UpdateTime  *gtime.Time            `json:"updateTime" orm:"update_time"`
	Remark      string                 `json:"remark"`
	DataScope   interface{}            `json:"dataScope"`
	Params      map[string]interface{} `json:"params"`
	DeptID      uint64                 `json:"deptId" orm:"dept_id"`
	ParentID    int64                  `json:"parentId" orm:"parent_id"`
	Ancestors   string                 `json:"ancestors" orm:"ancestors"`
	DeptName    string                 `json:"deptName" orm:"dept_name"`
	OrderNum    int                    `json:"orderNum" orm:"order_num" `
	Leader      string                 `json:"leader" orm:"leader"`
	Phone       string                 `json:"phone" orm:"phone"`
	Email       string                 `json:"email" orm:"email"`
	Status      string                 `json:"status" orm:"status"`
	DelFlag     string                 `json:"delFlag" orm:"del_flag" `
	ParentName  string                 `json:"parentName"`
	Children    []interface{}          `json:"children"`
}

//查询参数

//文章搜索参数
type SearchParams struct {
	DeptName string `p:"deptName"`
	Status   string `p:"status"`
}

type AddParams struct {
	ParentID   int         `json:"parentId"   orm:"parent_id"  p:"parentId"  v:"required#父级不能为空"`
	DeptName   string      `json:"deptName"   orm:"dept_name" p:"deptName"  v:"required#部门名称不能为空"`
	OrderNum   int         `json:"orderNum"   orm:"order_num"  p:"orderNum"  v:"required#排序不能为空"`
	Leader     string      `json:"leader"     orm:"leader" p:"leader"  v:"required#负责人不能为空"`
	Phone      string      `json:"phone"      orm:"Phone" p:"phone"  v:"required#电话不能为空"`
	Email      string      `json:"email"      orm:"email" p:"email"  v:"required#邮箱不能为空"`
	Status     string      `json:"status"     orm:"status" p:"status"  v:"required#状态必须"`
	Ancestors  string      `json:"ancestors"  orm:"ancestors"`
	DelFlag    string      `json:"delFlag"    orm:"del_flag"`
	CreateBy   string      `json:"createBy"   orm:"create_by"`
	CreateTime *gtime.Time `json:"createTime" orm:"create_time"`
	UpdateBy   string      `json:"updateBy"   orm:"update_by"`
	UpdateTime *gtime.Time `json:"updateTime" orm:"update_time"`
}

type EditParams struct {
	DeptID int64 `json:"deptId" orm:"dept_id" p:"id" v:"integer|min:1#ID只能为整数|ID只能为正数"`
	AddParams
}
