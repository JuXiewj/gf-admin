package sys_post

// Fill with you ideas below.

//type Post struct {
//	Entity
//	SearchValue interface{} `json:"searchValue"`
//	Remark      string      `json:"remark"`
//	DataScope   interface{} `json:"dataScope"`
//	Params      struct{}    `json:"params"`
//	Flag        bool        `json:"flag"`
//}

type SearchParams struct {
	PageNum  int    `p:"page"`     //当前页码
	PageSize int    `p:"pageSize"` //每页数
	PostCode string `p:"postCode"` //岗位编码
	PostName string `p:"postName"` //岗位名称
	Status   string `p:"status"`   //状态
}

type AddParams struct {
	PostCode string `p:"postCode" v:"required#岗位编码不能为空"`
	PostName string `p:"postName" v:"required#岗位名称不能为空"`
	PostSort int    `p:"postSort" v:"required#岗位排序不能为空"`
	Status   string `p:"status" v:"required#状态不能为空"`
	Remark   string `p:"remark"`
	AddUser  uint64
}

type EditParams struct {
	PostId int64 `p:"postId" v:"required#id必须"`
	AddParams
	UpUser uint64
}
