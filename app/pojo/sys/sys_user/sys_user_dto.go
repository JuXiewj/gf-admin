package sys_user

//设置用户状态参数
type StatusReq struct {
	Id         uint64 `p:"userId" v:"required#用户id不能为空"`
	UserStatus uint   `p:"status" v:"required#用户状态不能为空"`
}

//重置用户密码状态参数
type ResetPwdReq struct {
	Id       uint64 `p:"userId" v:"required#用户id不能为空"`
	Password string `p:"password" v:"required|password#密码不能为空|密码以字母开头，只能包含字母、数字和下划线，长度在6~18之间"`
}

//用户搜索请求参数
type SearchReq struct {
	DeptId      string `p:deptId` //部门id
	DeptIds     []int  //所属部门id数据
	BeginTime   string `p:"beginTime`
	EndTime     string `p:"endTime"`
	Phonenumber string `p:"phonenumber"`
	Status      string `p:"status"`
	KeyWords    string `p:"userName"`
	PageNum     int    `p:"page"`     //当前页码
	PageSize    int    `p:"pageSize"` //每页数
}

//添加修改用户公用请求字段
type SetUserReq struct {
	DeptId      uint64  `p:"deptId" v:"required#用户部门不能为空"` //所属部门
	Email       string  `p:"email" v:"email#邮箱格式错误"`       //邮箱
	NickName    string  `p:"nickName" v:"required#用户昵称不能为空"`
	Phonenumber string  `p:"phonenumber" v:"required|phone#手机号不能为空|手机号格式错误"`
	PostIds     []int64 `p:"postIds"`
	Remark      string  `p:"remark"`
	RoleIds     []int64 `p:"roleIds"`
	Sex         int     `p:"sex"`
	Status      uint    `p:"status"`
	IsAdmin     int     `p:"is_admin"` // 是否后台管理员 1 是  0   否
}

//添加用户请求
type AddUserReq struct {
	SetUserReq
	UserName string `p:"userName" v:"required#用户账号不能为空"`
	Password string `p:"password" v:"required|password#密码不能为空|密码以字母开头，只能包含字母、数字和下划线，长度在6~18之间"`
}

//修改用户请求
type EditUserReq struct {
	SetUserReq
	UserId int `p:"userId" v:"required#用户id不能为空"`
}
