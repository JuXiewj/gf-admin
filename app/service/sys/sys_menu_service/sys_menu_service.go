package sys_menu_service

import (
	"fmt"
	"gf-admin/app/dao/sys/sys_menu_dao"
	"gf-admin/app/pojo/sys/sys_menu"
	"gf-admin/app/service/casbin_adapter_service"
	"gf-admin/library/utils"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/text/gstr"
	"github.com/gogf/gf/util/gconv"
)

//获取MenuType==0,1菜单列表
func GetIsMenuList() ([]*sys_menu.Entity, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var gList = make([]*sys_menu.Entity, 0, len(list))
	for _, v := range list {
		if v.MenuType == 0 || v.MenuType == 1 {
			gList = append(gList, v)
		}
	}
	return gList, nil
}

//获取isMenu=0|1且status=1的菜单列表
func GetIsMenuStatusList() ([]*sys_menu.Entity, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var gList = make([]*sys_menu.Entity, 0, len(list))
	for _, v := range list {
		if (v.MenuType == 0 || v.MenuType == 1) && v.Status == 1 {
			gList = append(gList, v)
		}
	}
	return gList, nil
}

//获取所有按钮isMenu=2 且status=1的菜单列表
func GetIsButtonStatusList() ([]*sys_menu.Entity, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var gList = make([]*sys_menu.Entity, 0, len(list))
	for _, v := range list {
		if v.MenuType == 2 && v.Status == 1 {
			gList = append(gList, v)
		}
	}
	return gList, nil
}

//获取status==1的菜单列表
func GetMenuIsStatusList() ([]*sys_menu.Entity, error) {
	list, err := GetMenuList()
	if err != nil {
		return nil, err
	}
	var gList = make([]*sys_menu.Entity, 0, len(list))
	for _, v := range list {
		if v.Status == 1 {
			gList = append(gList, v)
		}
	}
	return gList, nil
}

//获取所有菜单
func GetMenuList() (list []*sys_menu.Entity, err error) {
	return sys_menu_dao.GetMenuList()
}

func GetMenuListSearch(req *sys_menu.ReqSearch) (list []*sys_menu.Entity, err error) {
	list, err = sys_menu_dao.GetMenuList()
	if err != nil {
		return
	}
	if req != nil {
		tmpList := make([]*sys_menu.Entity, 0, len(list))
		for _, entity := range list {
			if (req.Title == "" || gstr.Contains(gstr.ToUpper(entity.Title), gstr.ToUpper(req.Title))) &&
				(req.Status == "" || gconv.Uint(req.Status) == entity.Status) {
				tmpList = append(tmpList, entity)
			}
		}
		list = tmpList
	}
	return
}

//检查菜单规则是否存在
func CheckMenuNameUnique(name string, id int) bool {
	return sys_menu_dao.CheckMenuNameUnique(name, id)
}

//检查菜单路由地址是否已经存在
func CheckMenuPathUnique(path string, id int) bool {
	return sys_menu_dao.CheckMenuPathUnique(path, id)
}

// 添加菜单操作
func AddMenu(req *sys_menu.MenuReq) (err error, insertId int64) {
	return sys_menu_dao.Add(req)
}

//修改菜单操作
func EditMenu(req *sys_menu.MenuReq, id int) (err error, rows int64) {
	return sys_menu_dao.Edit(req, id)
}

//删除菜单
func DeleteMenuByIds(ids []int) (err error) {
	return sys_menu_dao.DeleteByIds(ids)
}

//获取菜单
func GetAllMenus() (menus g.List, err error) {
	//获取所有开启的菜单
	allMenus, err := GetIsMenuStatusList()
	if err != nil {
		return
	}
	menus = make(g.List, len(allMenus))
	for k, v := range allMenus {
		menu := gconv.Map(v)
		menu = setMenuMap(menu, v)
		menus[k] = menu
	}
	menus = utils.PushSonToParent(menus, 0, "pid", "id", "children", "", nil, true)
	return
}

//获取管理员所属角色菜单
func GetAdminMenusByRoleIds(roleIds []uint) (menus g.List, err error) {
	//获取角色对应的菜单id
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	menuIds := map[int64]int64{}
	for _, roleId := range roleIds {
		//查询当前权限
		gp := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", roleId))
		for _, p := range gp {
			mid := gconv.Int64(gstr.SubStr(p[1], 2))
			menuIds[mid] = mid
		}
	}
	//获取所有开启的菜单
	allMenus, err := GetIsMenuStatusList()
	if err != nil {
		return
	}
	roleMenus := make(g.List, 0, len(allMenus))
	for _, v := range allMenus {
		if _, ok := menuIds[gconv.Int64(v.Id)]; gstr.Equal(v.Condition, "nocheck") || ok {
			roleMenu := gconv.Map(v)
			roleMenu = setMenuMap(roleMenu, v)
			roleMenus = append(roleMenus, roleMenu)
		}
	}
	menus = utils.PushSonToParent(roleMenus, 0, "pid", "id", "children", "", nil, true)
	return
}


func GetPermissions(roleIds []uint) ([]string, error) {
	//获取角色对应的菜单id
	enforcer, err := casbin_adapter_service.GetEnforcer()
	if err != nil {
		return nil, err
	}
	menuIds := map[int64]int64{}
	for _, roleId := range roleIds {
		//查询当前权限
		gp := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", roleId))
		for _, p := range gp {
			mid := gconv.Int64(gstr.SubStr(p[1], 2))
			menuIds[mid] = mid
		}
	}
	//获取所有开启的按钮
	allButtons, err := GetIsButtonStatusList()
	userButtons := make([]string, 0, len(allButtons))
	for _, button := range allButtons {
		if _, ok := menuIds[gconv.Int64(button.Id)]; gstr.Equal(button.Condition, "nocheck") || ok {
			userButtons = append(userButtons, button.Name)
		}
	}
	return userButtons, nil
}

//组合返回menu前端数据
func setMenuMap(menu g.Map, entity *sys_menu.Entity) g.Map {
	menu["index"] = entity.Name
	menu["name"] = gstr.UcFirst(entity.Path)
	menu["menuName"] = entity.Title
	if entity.MenuType != 0 {
		menu["component"] = entity.Component
		menu["path"] = entity.Path
	} else {
		menu["path"] = "/" + entity.Path
		menu["component"] = "Layout"
	}
	menu["meta"] = g.MapStrStr{
		"icon":  entity.Icon,
		"title": entity.Title,
	}
	if entity.AlwaysShow == 1 {
		menu["hidden"] = false
	} else {
		menu["hidden"] = true
	}
	if entity.AlwaysShow == 1 && entity.MenuType == 0 {
		menu["alwaysShow"] = true
	} else {
		menu["alwaysShow"] = false
	}
	return menu
}