package sys_oper_log_service

import (
	"gf-admin/app/dao/sys/sys_oper_log_dao"
	"gf-admin/app/pojo/sys/sys_menu"
	"gf-admin/app/pojo/sys/sys_oper_log"
	"gf-admin/app/pojo/sys/sys_user"
	"github.com/gogf/gf/frame/g"
	"net/url"
)

//新增操作日志记录
func OperationLogAdd(user *sys_user.Entity, menu *sys_menu.Entity, url *url.URL,
	param g.Map, method, clientIp string) {
	sys_oper_log_dao.Add(user, menu, url, param, method, clientIp)
}

//操作日志列表
func OperationLogListByPage(req *sys_oper_log.SelectPageReq) (total, page int, list []*sys_oper_log.Entity, err error) {
	return sys_oper_log_dao.ListByPage(req)
}

//通过id获取操作日志
func GetOperationLogById(id int64) (log *sys_oper_log.Entity, err error) {
	return sys_oper_log_dao.GetById(id)
}

//批量删除
func DeleteOperationLogByIds(ids []int) (err error) {
	return sys_oper_log_dao.DeleteByIds(ids)
}

//清空
func ClearOperationLog() (err error) {
	return sys_oper_log_dao.ClearLog()
}
