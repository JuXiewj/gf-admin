package sys_dept_service

import (
	"database/sql"
	"gf-admin/app/dao/sys/sys_dept_dao"
	"gf-admin/app/dao/sys/sys_role_dept_dao"
	"gf-admin/app/pojo/sys/sys_dept"
)

/**
获取列表数据
*/
func GetList(searchParams *sys_dept.SearchParams) ([]*sys_dept.Dept, error) {
	if list, err := sys_dept_dao.GetList(searchParams); err != nil {
		return nil, err
	} else {
		return list, nil
	}
}

func GetRoleDepts(roleId int64) ([]int64, error) {
	return sys_role_dept_dao.GetRoleDepts(roleId)
}

func AddDept(data *sys_dept.AddParams) (sql.Result, error) {
	return sys_dept_dao.AddDept(data)
}

func EditDept(data *sys_dept.EditParams) error {
	return sys_dept_dao.EditDept(data)
}

func GetDeptById(id uint64) (*sys_dept.Dept, error) {
	return sys_dept_dao.GetDeptById(id)
}

/**
查询部门排除节点
*/
func Exclude(id int64) ([]*sys_dept.Dept, error) {
	return sys_dept_dao.Exclude(id)
}

/**
删除
*/
func DelDept(id int64) error {
	return sys_dept_dao.DelDept(id)
}
