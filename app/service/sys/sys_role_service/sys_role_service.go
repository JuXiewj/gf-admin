package sys_role_service

import (
	"fmt"
	"gf-admin/app/dao/sys/sys_role_dao"
	"gf-admin/app/pojo/sys/sys_role"
	"gf-admin/app/service/casbin_adapter_service"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/util/gconv"
)


//获取用户组(角色)列表
func GetRoleList() (list []*sys_role.Entity, err error) {
	return sys_role_dao.GetList()
}

func GetRoleListSearch(req *sys_role.SelectPageReq) (total, page int, list []*sys_role.Entity, err error) {
	return sys_role_dao.GetRoleListSearch(req)
}

//保存角色信息并返回插入的id
func AddRole(tx *gdb.TX, data map[string]interface{}) (InsId int64, err error) {
	return sys_role_dao.Add(tx, data)
}

//添加角色授权规则
func AddRoleRule(iRule interface{}, roleId int64) (err error) {
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	rule := gconv.Strings(iRule)
	for _, v := range rule {
		_, err = enforcer.AddPolicy(fmt.Sprintf("g_%d", roleId), fmt.Sprintf("r_%s", v), "All")
		if err != nil {
			return
		}
	}
	return
}

//修改角色信息操作
func EditRole(tx *gdb.TX, data map[string]interface{}) (err error) {
	return sys_role_dao.Edit(tx, data)
}

func StatusSetRole(req *sys_role.StatusSetReq) error {
	return sys_role_dao.StatusSetRole(req)
}

func RoleDataScope(req *sys_role.DataScopeReq) error {
	return sys_role_dao.DataScope(req)
}

//修改角色的授权规则
func EditRoleRule(iRule interface{}, roleId int64) (err error) {
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	//查询当前权限
	gp := enforcer.GetFilteredPolicy(0, fmt.Sprintf("g_%d", roleId))
	//删除旧权限
	for _, v := range gp {
		_, e = enforcer.RemovePolicy(v)
		if e != nil {
			err = e
			return
		}
	}
	rule := gconv.Strings(iRule)
	for _, v := range rule {
		_, err = enforcer.AddPolicy(fmt.Sprintf("g_%d", roleId), fmt.Sprintf("r_%s", v), "All")
		if err != nil {
			return
		}
	}
	return
}

//删除角色权限操作
func DeleteRoleRule(roleId int) (err error) {
	return sys_role_dao.DeleteRoleRule(roleId)
}

//添加用户角色信息
func AddUserRole(roleIds interface{}, userId int64) (err error) {
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	rule := gconv.Ints(roleIds)
	for _, v := range rule {
		_, err = enforcer.AddGroupingPolicy(fmt.Sprintf("u_%d", userId), fmt.Sprintf("g_%d", v))
		if err != nil {
			return
		}
	}
	return
}

//修改用户角色信息
func EditUserRole(roleIds interface{}, userId int) (err error) {
	enforcer, e := casbin_adapter_service.GetEnforcer()
	if e != nil {
		err = e
		return
	}
	rule := gconv.Ints(roleIds)
	//删除用户旧角色信息
	enforcer.RemoveFilteredGroupingPolicy(0, fmt.Sprintf("u_%d", userId))
	for _, v := range rule {
		_, err = enforcer.AddGroupingPolicy(fmt.Sprintf("u_%d", userId), fmt.Sprintf("g_%d", v))
		if err != nil {
			return
		}
	}
	return
}

func DeleteRoleByIds(ids []int) (err error) {
	return sys_role_dao.DeleteByIds(ids)
}

