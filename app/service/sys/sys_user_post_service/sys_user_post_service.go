package sys_user_post_service

import (
	"gf-admin/app/dao/sys/sys_user_post_dao"
	"gf-admin/app/pojo/sys/sys_post"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)

//添加用户岗位信息
func AddUserPost(postIds []int64, userId int64) (err error) {
	//删除旧岗位信息
	err = sys_user_post_dao.DeleteByUserId(userId)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("设置用户岗位信息失败")
	}
	//添加用户岗位信息
	err = sys_user_post_dao.AddUserPost(postIds, userId)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("设置用户岗位信息失败")
	}
	return
}

func GetPostsByUserId(id uint64) ([]*sys_post.Entity, error) {
	return sys_user_post_dao.GetPostsByUserId(id)
}