package gen_table_column_service

import (
	"gf-admin/app/dao/gen/gen_table_column_dao"
	"gf-admin/app/pojo/gen/gen_table_column"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)

func SelectDbTableColumnsByName(tableName string) ([]*gen_table_column.Entity, error) {
	return gen_table_column_dao.SelectDbTableColumnsByName(tableName)
}

//判断是否是数据库字符串类型
func IsStringObject(value string) bool {
	return gen_table_column_dao.IsStringObject(value)
}

//判断是否是数据库时间类型
func IsTimeObject(value string) bool {
	return gen_table_column_dao.IsTimeObject(value)
}

//判断是否是数据库数字类型
func IsNumberObject(value string) bool {
	return gen_table_column_dao.IsNumberObject(value)
}

//页面不需要编辑字段
func IsNotEdit(value string) bool {
	return gen_table_column_dao.IsNotEdit(value)
}

//页面不需要显示的列表字段
func IsNotList(value string) bool {
	return gen_table_column_dao.IsNotList(value)
}

//页面不需要查询字段
func IsNotQuery(value string) bool {
	return gen_table_column_dao.IsNotQuery(value)
}

//判断string 是否存在在数组中
func IsExistInArray(value string, array []string) bool {
	for _, v := range array {
		if v == value {
			return true
		}
	}
	return false
}

//查询业务字段列表
func SelectGenTableColumnListByTableId(tableId int64) ([]*gen_table_column.Entity, error) {
	list, err := gen_table_column.Model.Where(gen_table_column.Columns.TableId, tableId).Order(gen_table_column.Columns.Sort + " asc, " + gen_table_column.Columns.ColumnId + " asc").All()
	if err != nil {
		g.Log().Error(err)
		return nil, gerror.New("获取字段信息出错")
	}
	return list, nil
}