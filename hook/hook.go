package hook

import (
	"gf-admin/app/pojo/sys/sys_menu"
	"gf-admin/app/service/sys/sys_menu_service"
	"gf-admin/app/service/sys/sys_oper_log_service"
	"gf-admin/app/service/sys/sys_user_service"
	"gf-admin/library/utils"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/text/gstr"
)

func OperationLog(r *ghttp.Request) {
	user := sys_user_service.GetLoginAdminInfo(r)
	if user == nil {
		return
	}
	url := r.Request.URL //请求地址
	//获取菜单
	//获取地址对应的菜单id
	menuList, err := sys_menu_service.GetMenuList()
	if err != nil {
		g.Log().Error(err)
		return
	}
	var menu *sys_menu.Entity
	path := gstr.TrimLeft(url.Path, "/")
	for _, m := range menuList {
		if gstr.Equal(m.Name, path) {
			menu = m
			break
		}
	}
	go sys_oper_log_service.OperationLogAdd(user, menu, url, r.GetMap(), r.Method, utils.GetClientIp(r))
}
