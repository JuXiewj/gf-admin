package router

import (
	"gf-admin/app/api/gen/gen_table_controller"
	"gf-admin/app/api/sys/common_controller/captcha_controller"
	"gf-admin/app/api/sys/index_controller"
	"gf-admin/app/api/sys/server_controller"
	"gf-admin/app/api/sys/sys_config_controller"
	"gf-admin/app/api/sys/sys_dept_controller"
	"gf-admin/app/api/sys/sys_dict_controller"
	"gf-admin/app/api/sys/sys_job_controller"
	"gf-admin/app/api/sys/sys_login_log_controller"
	"gf-admin/app/api/sys/sys_menu_controller"
	"gf-admin/app/api/sys/sys_oper_log_controller"
	"gf-admin/app/api/sys/sys_post_controller"
	"gf-admin/app/api/sys/sys_role_controller"
	"gf-admin/app/api/sys/sys_user_controller"
	"gf-admin/app/api/sys/sys_user_online_controller"
	"gf-admin/app/api/sys/upload_controller"
	"gf-admin/hook"
	"gf-admin/middleWare"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

//后端路由处理
func init() {
	s := g.Server()
	group := s.Group("/")

	group.Group("/captcha", func(group *ghttp.RouterGroup) {
		group.ALL("/get", new(captcha_controller.Captcha))
	})

	group.Group("/system", func(group *ghttp.RouterGroup) {
		group.Middleware(middleWare.Auth) //后台权限验证
		//后台操作日志记录
		group.Hook("/*", ghttp.HookBeforeServe, hook.OperationLog)

		//文件上传
		group.POST("/upload", new(upload_controller.Upload))
		//后台首页
		group.ALL("/index", new(index_controller.Index))
		group.ALL("/menu", new(sys_menu_controller.Auth))
		group.ALL("/role", new(sys_role_controller.Auth))
		//部门管理
		group.ALL("/dept", new(sys_dept_controller.Dept))
		//个人中心
		group.ALL("/user", new(sys_user_controller.User))
		//岗位管理
		group.ALL("/post", new(sys_post_controller.Post))

		//配置管理
		group.Group("/config", func(group *ghttp.RouterGroup) {
			group.ALL("/dict", new(sys_dict_controller.Dict))
			group.ALL("/params", new(sys_config_controller.Params))
		})
		//系统监控
		group.Group("/monitor", func(group *ghttp.RouterGroup) {
			group.ALL("/online", new(sys_user_online_controller.MonitorOnline))
			group.ALL("/job", new(sys_job_controller.MonitorJob))
			group.ALL("/server", new(server_controller.MonitorServer))
			group.ALL("/operlog", new(sys_oper_log_controller.MonitorOperationLog))
			group.ALL("/loginlog", new(sys_login_log_controller.MonitorLoginLog))
		})

		//代码生成
		group.Group("/tools", func(group *ghttp.RouterGroup) {
			group.ALL("/gen", new(gen_table_controller.Gen))
		})

	})
}
