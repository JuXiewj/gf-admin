module gf-admin

go 1.15

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/casbin/casbin/v2 v2.19.8
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/goflyfox/gtoken v1.4.1
	github.com/gogf/gf v1.15.1
	github.com/mojocn/base64Captcha v1.3.1
	github.com/mssola/user_agent v0.5.2
	github.com/shirou/gopsutil v3.20.12+incompatible
	github.com/swaggo/swag v1.7.0
)
